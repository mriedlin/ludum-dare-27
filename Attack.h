#ifndef ATTACK_H
#define ATTACK_H

#include "Component.h"

struct Attack : public Component
{
  int value;
  int target;
  int range;

  Attack(int o,int tar,int val,int r):Component(o),value(val),target(tar),range(r){}
};

#endif
