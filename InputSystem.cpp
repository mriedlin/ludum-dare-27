#include <SFML/Graphics.hpp>
#include "EntitySystem.h"
#include "InputSystem.h"
#include "Component.h"
#include "Components.h"
#include "CompConstants.h"

InputSystem::InputSystem(sf::RenderWindow& w,int& e,EntitySystem& ee):
window(w),
exitGame(e),
es(ee)
{}

void InputSystem::update(double t)
{
  time += t;
  sf::Event event;
  while(window.pollEvent(event))
  {
    //close the game
    if(event.type == sf::Event::Closed) exitGame = 1;

    //mouse press events
    if(event.type == sf::Event::MouseButtonPressed)
    {
      if(event.mouseButton.button == sf::Mouse::Right)
      {
        double x = event.mouseButton.x;
        double y = event.mouseButton.y;
        for(unsigned int i=0;i<selected.size();i++)
        {
          Position* p = static_cast<Position*>(es.fetch(selected.at(i),position));
          Art* a = static_cast<Art*>(es.fetch(selected.at(i),art));
          sf::FloatRect r = a->art->getGlobalBounds();
          Order o;
          o.target = -1;
          o.time = time;
          o.dude = p->owner;
          o.x = x - r.width / 2.0;
          o.y = y - r.height / 2.0;
          for(unsigned int j=0;j<es.closedList.size();j++)
          {
            a = static_cast<Art*>(es.fetch(es.closedList.at(j),art));
            r = a->art->getGlobalBounds();
            if(r.contains(sf::Vector2f(x,y)))
            {
              Attack* atk = static_cast<Attack*>(es.fetch(p->owner,attack));
              int dude1 = static_cast<Player*>(es.fetch(atk->owner,player))->player;
              int dude2 = static_cast<Player*>(es.fetch(a->owner,player))->player;
              if(dude1 != dude2)
              {
                o.target = a->owner;
                break;
              }
            }
          }
          orders.push_back(o);
        }
      }
    }

    //mouse release events
    if(event.type == sf::Event::MouseButtonReleased)
    {
      //select single unit
      if(event.mouseButton.button == sf::Mouse::Left)
      {
        std::vector<Component*>& a = es.getRow(art);
        sf::Vector2f mp(event.mouseButton.x,event.mouseButton.y);
        for(unsigned int i=0;i<a.size();i++)
        {
          if(a.at(i) != NULL)
          {
            sf::FloatRect bb = static_cast<Art*>(a.at(i))->art->getGlobalBounds();
            int p = static_cast<Player*>(es.fetch((a.at(i))->owner,player))->player;
            if(bb.contains(mp) && p == 1)
            {
              selected.clear();
              selected.push_back(a.at(i)->owner);
            }
          }
        }
      }
    }
  }
  if(!orders.empty())
  {
    while(((time - orders.front().time) > 10) && orders.size() > 0)
    {
      if(orders.empty()) break;
      Order o(orders.front());
      Position* p = static_cast<Position*>(es.fetch(o.dude,position));
      Attack* a = static_cast<Attack*>(es.fetch(o.dude,attack));
      p->xd = o.x;
      p->yd = o.y;
      a->target = o.target;
      orders.pop_front();
    }
  }
}
