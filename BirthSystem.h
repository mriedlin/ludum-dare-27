#ifndef BIRTHSYSTEM_H
#define BIRTHSYSTEM_H

#include <vector>

struct Birth;

class EntitySystem;

class BirthSystem
{
  public:
  BirthSystem(EntitySystem& e,double t);
  void update(double t);

  private:
  void createEntity(Birth* b);
  void createMelee(Birth* b);

  EntitySystem& es;
  double threshhold;
  double time;
};

#endif
