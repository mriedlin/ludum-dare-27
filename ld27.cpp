//includes
#include <iostream>
#include <vector>
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

//components
#include "Components.h"

//systems
#include "InputSystem.h"
#include "EntitySystem.h"
#include "RenderSystem.h"
#include "MovementSystem.h"
#include "BirthSystem.h"
#include "CombatSystem.h"
#include "AISystem.h"
#include "CompConstants.h"

//global constants
int exitGame = -1;
int gameOver = -1;

const char* title = "Ludum Dare 27";

void checkHealth(EntitySystem& es);

//main
int main(void)
{
  //setup
  sf::RenderWindow window(sf::VideoMode(800,600), title);

  //entities and components
  EntitySystem es;
  es.initialEntities(128);
  const int POSITION = es.addComp(position);
  const int SPEED = es.addComp(speed);
  const int ART = es.addComp(art);
  const int BIRTH = es.addComp(birth);
  const int HEALTH = es.addComp(health);
  const int PLAYER = es.addComp(player);
  const int ATTACK = es.addComp(attack);
  const int DEFENSE = es.addComp(defense);
  es.initialize();

  int fact = es.addEntity();
  es.attach(fact,position,new Position(fact, 50, 500));
  es.attach(fact,art,new Art(fact, new sf::RectangleShape(sf::Vector2f(80,40))));
  es.attach(fact,birth,new Birth(fact, 1, 1,80,490));
  es.attach(fact,health, new Health(fact, 500));
  es.attach(fact,player, new Player(fact, 1));
  static_cast<Art*>(es.fetch(fact,art))->art->setFillColor(sf::Color::Blue);

  fact = es.addEntity();
  es.attach(fact,position,new Position(fact, 150, 500));
  es.attach(fact,art,new Art(fact, new sf::RectangleShape(sf::Vector2f(80,40))));
  es.attach(fact,birth,new Birth(fact, 1, 1,180,490));
  es.attach(fact,health, new Health(fact, 500));
  es.attach(fact,player, new Player(fact, 1));
  static_cast<Art*>(es.fetch(fact,art))->art->setFillColor(sf::Color::Blue);

  fact = es.addEntity();
  es.attach(fact,position,new Position(fact, 250, 500));
  es.attach(fact,art,new Art(fact, new sf::RectangleShape(sf::Vector2f(80,40))));
  es.attach(fact,birth,new Birth(fact, 1, 1,280,490));
  es.attach(fact,health, new Health(fact, 500));
  es.attach(fact,player, new Player(fact, 1));
  static_cast<Art*>(es.fetch(fact,art))->art->setFillColor(sf::Color::Blue);

  fact = es.addEntity();
  es.attach(fact,position,new Position(fact, 670, 60));
  es.attach(fact,art,new Art(fact, new sf::RectangleShape(sf::Vector2f(80,40))));
  es.attach(fact,birth,new Birth(fact, 1, 1,710,100));
  es.attach(fact,health, new Health(fact, 500));
  es.attach(fact,player, new Player(fact, 2));
  static_cast<Art*>(es.fetch(fact,art))->art->setFillColor(sf::Color::Red);

  fact = es.addEntity();
  es.attach(fact,position,new Position(fact, 570, 60));
  es.attach(fact,art,new Art(fact, new sf::RectangleShape(sf::Vector2f(80,40))));
  es.attach(fact,birth,new Birth(fact, 1, 1,610,100));
  es.attach(fact,health, new Health(fact, 500));
  es.attach(fact,player, new Player(fact, 2));
  static_cast<Art*>(es.fetch(fact,art))->art->setFillColor(sf::Color::Red);

  fact = es.addEntity();
  es.attach(fact,position,new Position(fact, 470, 60));
  es.attach(fact,art,new Art(fact, new sf::RectangleShape(sf::Vector2f(80,40))));
  es.attach(fact,birth,new Birth(fact, 1, 1,510,100));
  es.attach(fact,health, new Health(fact, 500));
  es.attach(fact,player, new Player(fact, 2));
  static_cast<Art*>(es.fetch(fact,art))->art->setFillColor(sf::Color::Red);

  //system initialization
  InputSystem iosys(window,exitGame,es);
  RenderSystem render(window, es);
  MovementSystem movesys(es);
  BirthSystem birth(es, 12);
  CombatSystem combat(es);
  AISystem ai(es,2);

  sf::Clock clock;
  sf::Time elapsed = clock.restart();
  //main loop
  while(exitGame == -1)
  {
    elapsed = clock.restart();
    render.update();
    iosys.update(elapsed.asSeconds());
    ai.update(elapsed.asSeconds());
    movesys.update(elapsed.asSeconds());
    birth.update(elapsed.asSeconds());
    combat.update(elapsed.asSeconds());
    checkHealth(es);
  }

  //cleanup
  window.close();
  return 0;
}

void checkHealth(EntitySystem& es)
{
  std::vector<Component*>& hp = es.getRow(health);
  for(unsigned int i=0;i<hp.size();i++)
  {
    if(hp.at(i) != NULL)
    {
      if(static_cast<Health*>(hp.at(i))->hp < 1)
      {
        es.removeEntity(hp.at(i)->owner);
      }
    }
  }
}
