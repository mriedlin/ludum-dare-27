#ifndef PLAYER_H
#define PLAYER_H

#include "Component.h"

struct Player : public Component
{
  const int player;

  Player(int o, int p):Component(o),player(p){}
};

#endif
