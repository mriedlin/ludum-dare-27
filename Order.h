#ifndef ORDER_H
#define ORDER_H

struct Order
{
  double x;
  double y;
  double time;
  int target;
  int dude;

  Order(){}
  Order(const Order& o):x(o.x),y(o.y),time(o.time),target(o.target),dude(o.dude){}
  

};

#endif
