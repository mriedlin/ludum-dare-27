#ifndef BIRTH_H
#define BIRTH_H

#include "Component.h"

struct Birth : public Component
{
  int type;
  int num;
	double sx;
	double sy;

  Birth(int o,int t, int n,double x,double y):
	Component(o),
	type(t),
	num(n),
	sx(x),
	sy(y){}
};

#endif
