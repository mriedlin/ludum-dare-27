#ifndef INPUTSYSTEM_H
#define INPUTSYSTEM_H

#include <vector>
#include <list>
#include "Order.h"

class EntitySystem;

class InputSystem
{
  public:
  InputSystem(sf::RenderWindow& w,int& exitGame,EntitySystem& e);
  void update(double t);

  private:
  sf::RenderWindow& window;
  int& exitGame;
  EntitySystem& es;
  double time;
  std::vector<int> selected;
  std::list<Order> orders;
};

#endif
