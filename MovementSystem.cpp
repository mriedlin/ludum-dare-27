#include <cmath>
#include <SFML/Graphics.hpp>
#include "MovementSystem.h"
#include "EntitySystem.h"
#include "Components.h"
#include "CompConstants.h"

MovementSystem::MovementSystem(EntitySystem& e):es(e){}

void MovementSystem::update(double elapsed)
{
  Position* p = NULL;
  Speed* s = NULL;
  for(unsigned int i=0;i<es.closedList.size();i++)
  {
    p = static_cast<Position*>(es.fetch(es.closedList.at(i),position));
    s = static_cast<Speed*>(es.fetch(es.closedList.at(i),speed));
    if(p != NULL && s != NULL)
    {
      if(p->x != p->xd || p->y != p->yd)
      {
        double vx = p->xd - p->x;
        double vy = p->yd - p->y;
        double d = sqrt(vx*vx + vy*vy);
        double nx = (vx/d) * s->s * elapsed;
        double ny = (vy/d) * s->s * elapsed;
        if(sqrt(nx*nx + ny*ny) > d)
        {
          p->x = p->xd;
          p->y = p->yd;
        }
        else
        {
          p->x = p->x + nx;
          p->y = p->y + ny;
        }
      }
    }
  }
  //collision resolution
  for(unsigned int i=0;i<es.closedList.size();i++)
  {
    Art* a = static_cast<Art*>(es.fetch(es.closedList.at(i),art));
    for(unsigned int j=0;j<es.closedList.size();j++)
    {
      if(j != i)
      {
        Art* b = static_cast<Art*>(es.fetch(es.closedList.at(j),art));
        if((a->art->getGlobalBounds()).intersects(b->art->getGlobalBounds()))
        {
          if(es.fetch(a->owner,speed) == NULL)
          {
            Art *s = a;
            a = b;
            b = s;
          }
          sf::FloatRect abox = a->art->getGlobalBounds();
          sf::FloatRect bbox = b->art->getGlobalBounds();
          if(abs((bbox.top - abox.top) / (bbox.left - abox.left)) < 1)
          {
           if(abox.left < bbox.left) //a is left of b
            {
              Position* l = static_cast<Position*>(es.fetch(a->owner,position));
              l->x = bbox.left - abox.width;
              l->xd = l->x;
              l->yd = l->y;
            }
            else
            {
              Position* l = static_cast<Position*>(es.fetch(a->owner,position));
              l->x = bbox.left + bbox.width;
              l->xd = l->x;
              l->yd = l->y;
            }
          }
          else
          {
            if(abox.top < bbox.top) //a is above b
            {
              Position* l = static_cast<Position*>(es.fetch(a->owner,position));
              l->y = bbox.top - abox.height;
              l->yd = l->y;
              l->xd = l->x;
            }
            else
            {
              Position* l = static_cast<Position*>(es.fetch(a->owner,position));
              l->y = bbox.top + bbox.height;
              l->yd = l->y;
              l->xd = l->x;
            }
          }
        }
      }
    }
  }
}

