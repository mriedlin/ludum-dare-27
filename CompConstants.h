#ifndef COMPCONSTANTS_H
#define COMPCONSTANTS_H

#include <string>

const std::string position("position");
const std::string speed("speed");
const std::string art("art");
const std::string birth("birth");
const std::string health("health");
const std::string player("player");
const std::string attack("attack");
const std::string defense("defense");

#endif
