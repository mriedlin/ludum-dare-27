#include <SFML/Graphics.hpp>
#include "BirthSystem.h"
#include "EntitySystem.h"
#include "Components.h"
#include "CompConstants.h"

BirthSystem::BirthSystem(EntitySystem& e,double t):es(e),threshhold(t),time(100){};

void BirthSystem::update(double t)
{
  time += t;
  if(time >= threshhold)
  {
    std::vector<Component*>& factories = es.getRow(birth);
    for(unsigned int i=0;i<factories.size();i++)
    {
      if(factories.at(i) != NULL)
      {
        createEntity(static_cast<Birth*>(factories.at(i)));
      }
    }
    time = 0;
  }
}

void BirthSystem::createEntity(Birth* b)
{
  int type = b->type;
  sf::FloatRect szone(b->sx,b->sy,10,10);
  for(unsigned int i=0;i<es.getRow(art).size();i++)
  {
    Art* a = static_cast<Art*>(es.getRow(art).at(i));
    if(a != NULL)
    {
      if(szone.intersects(a->art->getGlobalBounds())) return;
    }
  }
  if(type == 1) createMelee(b);
}

void BirthSystem::createMelee(Birth* b)
{
  int target = es.addEntity();
  es.attach(target,position, new Position(target,b->sx,b->sy));
  es.attach(target,speed,new Speed(target,100.0));
  es.attach(target,art,new Art(target,new sf::RectangleShape(sf::Vector2f(10,10))));
  es.attach(target,health,new Health(target,30));
  es.attach(target,attack,new Attack(target,-1,10,5));
  es.attach(target,defense,new Defense(target,6));
  es.attach(target,player,new Player(target,static_cast<Player*>(es.fetch(b->owner,player))->player));
  static_cast<Art*>(es.fetch(target,art))->art->setFillColor(static_cast<Art*>(es.fetch(b->owner,art))->art->getFillColor());
}
