#include <SFML/Graphics.hpp>
#include "CombatSystem.h"
#include "EntitySystem.h"
#include "Components.h"
#include "CompConstants.h"

CombatSystem::CombatSystem(EntitySystem& e):es(e),sec(0){}

void CombatSystem::update(double time)
{
  sec += time;
  if(sec < 1) return;
  sec = 0;
  for(unsigned int i=0;i<es.closedList.size();i++)
  {
    Attack* atk = static_cast<Attack*>(es.fetch(es.closedList.at(i),attack));
    if(atk != NULL)
    {
      if(atk->target > -1)
      {
        //Defense* def = static_cast<Defense*>(es.fetch(atk->target,defense));
        Health* hp = static_cast<Health*>(es.fetch(atk->target,health));
        if(hp != NULL)
        {
          if(inRange(atk,hp))
          {
            Defense* def = static_cast<Defense*>(es.fetch(atk->target,defense));
            if(def == NULL)
            {
              hp->hp = hp->hp - atk->value;
            }
            else
            {
              hp->hp = hp->hp - (atk->value - def->value);
            }
          }
        }
        else
        {
          atk->target = -1;
        }
      }
    }
  }
}

bool CombatSystem::inRange(Attack* a, Health* d)
{
  double r = static_cast<double>(a->range);
  Art* artp = static_cast<Art*>(es.fetch(a->owner,art));
  sf::FloatRect atkBox = artp->art->getGlobalBounds();
  atkBox.left = atkBox.left - r;
  atkBox.top = atkBox.top - r;
  atkBox.width = atkBox.width + 2*r;
  atkBox.width = atkBox.height + 2*r;
  artp = static_cast<Art*>(es.fetch(d->owner,art));
  if(atkBox.intersects(artp->art->getGlobalBounds())) return true;
  else return false;
}
