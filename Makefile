CC = g++
LIBS = -L/usr/local/lib -lsfml-graphics -lsfml-window -lsfml-system
FLAGS = -c -Wall -g
OBJECTS = ld27.o InputSystem.o EntitySystem.o RenderSystem.o MovementSystem.o BirthSystem.o CombatSystem.o AISystem.o

ld27 : $(OBJECTS)
	$(CC) $(OBJECTS) -o ld27 $(LIBS)

InputSystem.o : InputSystem.cpp
	$(CC) InputSystem.cpp $(FLAGS)

ld27.o : ld27.cpp
	$(CC) ld27.cpp $(FLAGS)

EntitySystem.o : EntitySystem.cpp
	$(CC) EntitySystem.cpp $(FLAGS)

RenderSystem.o : RenderSystem.cpp
	$(CC) RenderSystem.cpp $(FLAGS)

MovementSystem.o : MovementSystem.cpp
	$(CC) MovementSystem.cpp $(FLAGS)

BirthSystem.o : BirthSystem.cpp
	$(CC) BirthSystem.cpp $(FLAGS)

CombatSystem.o : CombatSystem.cpp
	$(CC) CombatSystem.cpp $(FLAGS)

AISystem.o : AISystem.cpp
	$(CC) AISystem.cpp $(FLAGS)

clean :
	rm *.o ld27
