#ifndef COLLISIONSYSTEM_H
#define COLLISIONSYSTEM_H

class EntitySystem;

class CollisionSystem
{
	public:
	CollisionSystem(EntitySystem& e);
	void update()
};

#endif
