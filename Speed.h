#ifndef SPEED_H
#define SPEED_H

#include "Component.h"

struct Speed : public Component
{
  float s;

  Speed(int o, float _s):Component(o),s(_s){}
};

#endif
