#ifndef RENDERSYSTEM_H
#define RENDERSYSTEM_H

class EntitySystem;

class RenderSystem
{
  public:
  RenderSystem(sf::RenderWindow& w, EntitySystem& e);
  void update();

  private:
  sf::RenderWindow& window;
  EntitySystem& es;
};

#endif
