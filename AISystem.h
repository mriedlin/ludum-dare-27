#ifndef AISYSTEM_H
#define AISYSTEM_H

class EntitySystem;

class AISystem
{
  public:
  AISystem(EntitySystem& e, int p);
  void update(double);

  private:
  EntitySystem& es;
  int play;
  double time;
};

#endif
