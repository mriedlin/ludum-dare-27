#ifndef DEFENSE_H
#define DEFENSE_H

#include "Component.h"

struct Defense : public Component
{
  int value;

  Defense(int o,int val):Component(o),value(val){};
};

#endif
