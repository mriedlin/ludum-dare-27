#ifndef COMPONENT_H
#define COMPONENT_H

struct Component
{
  int owner;

  Component(int o):owner(o){}
};

#endif
