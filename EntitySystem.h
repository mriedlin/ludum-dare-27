#ifndef ENTITYSYSTEM_H
#define ENTITYSYSTEM_H

#include <vector>
#include <string>

struct Component;

class EntitySystem
{
  public:
  EntitySystem();
  ~EntitySystem();
  int addComp(const std::string& n);
  void initialEntities(int num);
  bool initialize();
  int addEntity();
  void attach(int eid,int type,Component* c);
  void attach(int eid,const std::string& type, Component* c);
  Component* fetch(int eid, int type);
  Component* fetch(int eid, const std::string& type);
  std::vector<Component*>& getRow(const std::string& type);
  int getMaxId();
  void removeEntity(int id);

  std::vector<int> closedList;

  private:
  int numComponents;
  int maxEid;
  std::vector<std::vector<Component*> > components;
  std::vector<int> openList;
  std::vector<std::string> cNames;
};

#endif 
