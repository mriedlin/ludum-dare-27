#ifndef POSITION_H
#define POSITION_H

#include "Component.h"

struct Position : public Component
{
  double x;
  double y;
  double xd;
  double yd;

  Position(int o, double _x, double _y):Component(o),x(_x),y(_y),xd(_x),yd(_y){};
};

#endif
