#ifndef MOVEMENTSYSTEM_H
#define MOVEMENTSYSTEM_H

class EntitySystem;

class MovementSystem
{
  public:
  MovementSystem(EntitySystem& e);
  void update(double e);

  private:
  EntitySystem& es;

};

#endif
