#include <SFML/Graphics.hpp>
#include "AISystem.h"
#include "EntitySystem.h"
#include "Components.h"
#include "CompConstants.h"

AISystem::AISystem(EntitySystem& e, int p):es(e),play(p),time(0){}

//issue commands......
void AISystem::update(double t)
{
  time += t;
  if(time >= 10)
  {
    for(unsigned int i=0;i<es.closedList.size();i++)
    {
      Player* p = static_cast<Player*>(es.fetch(es.closedList.at(i),player));
      Attack* a = static_cast<Attack*>(es.fetch(es.closedList.at(i),attack));
      if(p != NULL && a != NULL && p->player != 1)
      {
        std::vector<Component*>& facts = es.getRow(birth);
        for(unsigned int j=0;j<facts.size();j++)
        {
          Birth* b = static_cast<Birth*>(facts.at(j));
          if(b != NULL)
          {
            int pp = static_cast<Player*>(es.fetch(b->owner,player))->player;
            if(pp != p->player)
            {
              a->target = b->owner;
              Position* aip = static_cast<Position*>(es.fetch(a->owner,position));
              Position* tp = static_cast<Position*>(es.fetch(b->owner,position));
              aip->xd = tp->x;
              aip->yd = tp->y;
            }
          }
        }
      }
    }
    time = 0;
  }
}

