#ifndef ART_H
#define ART_H

#include "Component.h"

struct Art : public Component
{
  sf::Shape* art;

  Art(int o, sf::Shape* a):Component(o),art(a){}
  ~Art() { delete art;}
};

#endif
