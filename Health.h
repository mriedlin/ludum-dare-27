#ifndef HEALTH_H
#define HEALTH_H

#include "Component.h"

struct Health : public Component
{
  int hp;

  Health(int o, int h):Component(o),hp(h){}
};

#endif
