#include <SFML/Graphics.hpp>

#include "RenderSystem.h"
#include "EntitySystem.h"
#include "Art.h"
#include "Position.h"
#include "CompConstants.h"

RenderSystem::RenderSystem(sf::RenderWindow& w, EntitySystem& e):
window(w),
es(e)
{}

void RenderSystem::update()
{
  window.clear(sf::Color::Green);

  for(unsigned int i=0;i<es.closedList.size();i++)
  {
    Art* a = static_cast<Art*>(es.fetch(es.closedList[i],art));
    Position* p = static_cast<Position*>(es.fetch(es.closedList[i],position));
    if(a != NULL)
    {
      if(p != NULL)
      {
        a->art->setPosition(p->x,p->y);
      }
      window.draw(*(a->art));
    }
  }

  window.display();
}
