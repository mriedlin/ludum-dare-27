#include <algorithm>
#include "EntitySystem.h"
#include "Component.h"

EntitySystem::EntitySystem():numComponents(0),maxEid(-1){}

EntitySystem::~EntitySystem()
{
  for(unsigned int i=0;i<components.size();i++)
  {
    for(unsigned int j=0;j<components.at(i).size();j++)
    {
      if(components.at(i).at(j) != NULL)
      {
        delete components.at(i).at(j);
        components.at(i).at(j) = NULL;
      }
    }
  }
}

void EntitySystem::removeEntity(int id)
{
  for(unsigned int i=0;i<components.size();i++)
  {
    if(components.at(i).at(id) != NULL) 
    {
      delete components.at(i).at(id);
      components.at(i).at(id) = NULL;
    }
  }
  openList.push_back(id);
  closedList.erase(std::remove(closedList.begin(),closedList.end(),id),closedList.end());
}

int EntitySystem::addComp(const std::string& n)
{
  numComponents += 1;
  cNames.push_back(n);
  return cNames.size() - 1;
}

void EntitySystem::initialEntities(int num)
{
  if(maxEid == -1) maxEid = num;
}

bool EntitySystem::initialize()
{
  if(maxEid < 0 || numComponents < 1) return false;
  components.resize(numComponents);
  for(unsigned int i = 0;i<components.size();i++)
  {
    components[i].resize(maxEid);
    for(unsigned int j = 0;j<components[i].size();j++)
    {
      components.at(i).at(j) = NULL;
    }
  }
  for(int i=0;i<maxEid;i++) openList.push_back(i);
  return true;
}

int EntitySystem::addEntity()
{
  if(!openList.empty())
  {
    int eid = openList.back();
    closedList.push_back(eid);
    openList.pop_back();
    return eid;
  }
  else
  {
    for(int i = maxEid; i<maxEid*2;i++)
    {
      openList.push_back(i);
    }
    maxEid = maxEid * 2;
    for(unsigned int i = 0; i<components.size();i++)
    {
      components[i].resize(maxEid,NULL);
    }
    int eid = openList.back();
    openList.pop_back();
    return eid;
  }
}

void EntitySystem::attach(int eid, int type, Component* c)
{
  components.at(type).at(eid) = c;
}

void EntitySystem::attach(int eid,const std::string& t, Component* c)
{
  int type = -1;
  for(unsigned int i=0;i<cNames.size();i++)
  {
    if(cNames.at(i).compare(t) == 0) type = i;
  }
  if(type == -1) return;
  components.at(type).at(eid) = c;
}

Component* EntitySystem::fetch(int eid, int type)
{
  return components.at(type).at(eid);
}

Component* EntitySystem::fetch(int eid, const std::string& type)
{
  for(unsigned int i = 0;i<cNames.size();i++)
  {
    if(cNames[i].compare(type) == 0)
    {
      return components.at(i).at(eid);
    }
  }
  return NULL;
}

std::vector<Component*>& EntitySystem::getRow(const std::string& type)
{
  for(unsigned int i=0;i<cNames.size();i++)
  {
    if(type.compare(cNames.at(i)) == 0)
    {
      return components.at(i);
    }
  }
}

int EntitySystem::getMaxId()
{
  return maxEid;
}
