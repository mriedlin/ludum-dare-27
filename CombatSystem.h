#ifndef COMBATSYSTEM_H
#define COMBATSYSTEM_H

struct Attack;
struct Health;
class EntitySystem;

class CombatSystem
{
  public:
  CombatSystem(EntitySystem& e);
  void update(double time);

  private:
  EntitySystem& es;
  double sec;
  bool inRange(Attack* a,Health* d);
};

#endif
