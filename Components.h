#ifndef COMPONENTS_H
#define COMPONENTS_H

#include "Position.h"
#include "Speed.h"
#include "Art.h"
#include "Birth.h"
#include "Health.h"
#include "Player.h"
#include "Attack.h"
#include "Defense.h"

#endif
